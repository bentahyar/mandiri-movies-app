//
//  Review.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation

struct Review: Codable {
    let id: String?
    let author: String?
    let content: String?
    let createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id, author, content
        case createdAt = "created_at"
    }
}
