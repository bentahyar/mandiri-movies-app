//
//  DetailApi.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

protocol DetailApi {
    func getReviews(movieId: String, pageNum: Int, pageSize: Int) -> Observable<[Review]>
    func getVideos(movieId: String) -> Observable<[Video]>
}
