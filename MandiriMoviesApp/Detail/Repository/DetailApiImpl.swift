//
//  DetailApiImpl.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift
import Alamofire

class DetailApiImpl: DetailApi {
    private let disposeBag = DisposeBag()
    
    func getReviews(movieId: String, pageNum: Int, pageSize: Int) -> Observable<[Review]> {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/\(movieId)/reviews") else {
            return .empty()
        }
        
        let params: Parameters = ["page": pageNum,
                                  "pageSize": pageSize]
        
        return ApiInterface.shared.makeRequest(url: url, method: .get, parameter: params)
            .flatMap { jsonData -> Observable<[Review]> in
                let datum: DataCodableResponse<Review> = try JSONDecoder().decode(DataCodableResponse.self, from: jsonData)
                return Observable.just(datum.results ?? [])
        }
    }
    
    func getVideos(movieId: String) -> Observable<[Video]> {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/\(movieId)/videos") else {
            return .empty()
        }
        
        return ApiInterface.shared.makeRequest(url: url, method: .get, parameter: nil)
            .flatMap { jsonData -> Observable<[Video]> in
                let datum: DataCodableResponse<Video> = try JSONDecoder().decode(DataCodableResponse.self, from: jsonData)
                return Observable.just(datum.results ?? [])
        }
    }
}
