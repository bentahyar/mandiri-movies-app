//
//  Video.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation

struct Video: Codable {
    let id: String?
    let type: String?
    let official: Bool?
    let key: String?
}
