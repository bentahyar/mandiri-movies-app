//
//  MainApiImpl.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift
import Alamofire

class MainApiImpl: MainApi {
    private let disposeBag = DisposeBag()
    
    func getGenres() -> Observable<[Genres]> {
        guard let url = URL(string: "https://api.themoviedb.org/3/genre/movie/list?language=en") else {
            return .empty()
        }
        
        return ApiInterface.shared.makeRequest(url: url, method: .get, parameter: nil)
            .flatMap { jsonData -> Observable<[Genres]> in
                let datum: DataCodableResponse<Genres> = try JSONDecoder().decode(DataCodableResponse.self, from: jsonData)
                return Observable.just(datum.genres ?? [])
        }
    }
}
