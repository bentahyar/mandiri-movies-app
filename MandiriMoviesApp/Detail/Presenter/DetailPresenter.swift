//
//  DetailPresenter.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

class DetailPresenter {
    private let detailInteractor: DetailInteractor
    private let disposeBag = DisposeBag()
    
    private(set) var movieId = ""
    private(set) var pageNumber = 0
    
    private let eventShowReviews = PublishSubject<[Review]>()
    var rxEventShowReviews: Observable<[Review]> {
        return eventShowReviews
    }
    
    private let eventShowYoutube = PublishSubject<Video>()
    var rxEventShowYoutube: Observable<Video> {
        return eventShowYoutube
    }
    
    public init(detailInteractor: DetailInteractor) {
        self.detailInteractor = detailInteractor
    }
    
    public func setId(movieId: String) {
        self.movieId = movieId
    }
    
    func getReviews(pageNum: Int = 1, pageSize: Int = 10) {
        self.detailInteractor.getReviews(movieId: movieId, pageNum: pageNum, pageSize: pageSize)
            .subscribe(onNext: { [weak self] reviews in
                guard let self else { return }
                guard !reviews.isEmpty else { return }
                
                self.eventShowReviews.onNext(reviews)
                self.pageNumber += 1
            }, onError: { _ in
                
            })
            .disposed(by: disposeBag)
    }
    
    func getYoutubeVideo() {
        self.detailInteractor.getVideos(movieId: movieId)
            .subscribe(onNext: { [weak self] videos in
                guard let self else { return }
                guard !videos.isEmpty else { return }
                guard let video = videos.first(where: { $0.official! && $0.type == "Trailer" }) else { return }
                
                self.eventShowYoutube.onNext(video)
            }, onError: { _ in
                
            })
            .disposed(by: disposeBag)
    }
}
