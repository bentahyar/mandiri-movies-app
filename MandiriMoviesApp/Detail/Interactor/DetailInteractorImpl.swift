//
//  DetailInteractorImpl.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

class DetailInteractorImpl: DetailInteractor {
    private let detailApi: DetailApi
    
    public init(detailApi: DetailApi) {
        self.detailApi = detailApi
    }
    
    func getReviews(movieId: String, pageNum: Int, pageSize: Int) -> Observable<[Review]> {
        return self.detailApi.getReviews(movieId: movieId, pageNum: pageNum, pageSize: pageSize)
    }
    
    func getVideos(movieId: String) -> Observable<[Video]> {
        return self.detailApi.getVideos(movieId: movieId)
    }
}
