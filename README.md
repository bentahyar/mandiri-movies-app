
# Mandiri Movies App
This is a readme documentation for Mandiri Movies App, to help the appraiser to better understand what have been done through the stages of this project.

# Architecture
The architecture that is used is modified VIPER, where we change the router to use repository. The view and view controller itself are responsible for the views and UI, the presenter acts as a middleware and interactor as a bridge to repository. The repository will handle the business logic (connecting to API or reading from core data). 

This makes the project readable and testable through the use of architecture. Using architecture is one of the way to make our project abide to SOLID principle.

# Dependency Injection (DI)
This project also used dependency injection so the class is independent of its dependencies. This again helps the project to abide to SOLID principle.

# Dependency (Pods)
This project use several libraries to help it's function. The libraries are :
-  pod 'Alamofire' -> to help fetching API
-  pod 'RxSwift' -> to help use reactive functions
-  pod 'RxCocoa' -> to help use reactive functions in views
-  pod 'Swinject' -> to help use dependency injection
-  pod 'SwinjectStoryboard' -> to help use dependency injection
-  pod 'youtube-ios-player-helper' -> to load youtube url

# Unit Test
Unit test is not added due to time
