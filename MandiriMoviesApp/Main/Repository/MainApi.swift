//
//  MainApi.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

protocol MainApi {
    func getGenres() -> Observable<[Genres]>
}
