//
//  Movie.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation

struct Movie: Codable {
    let id: Int?
    let title: String?
    let overview: String?
    let popularity: Double?
    let releaseDate: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, overview, popularity
        case releaseDate = "release_date"
    }
}
