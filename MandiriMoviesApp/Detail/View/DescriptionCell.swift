//
//  DescriptionCell.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import UIKit

class DescriptionCell: UITableViewCell {

    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        return label
    }()

    let popularityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .gray
        return label
    }()

    let releaseDateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .gray
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Add subviews and setup constraints
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }

    private func setupViews() {
        // Add subviews
        addSubview(descriptionLabel)
        addSubview(popularityLabel)
        addSubview(releaseDateLabel)

        // Set up constraints
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),

            popularityLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 8),
            popularityLabel.leadingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor),
            popularityLabel.trailingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor),

            releaseDateLabel.topAnchor.constraint(equalTo: popularityLabel.bottomAnchor, constant: 8),
            releaseDateLabel.leadingAnchor.constraint(equalTo: popularityLabel.leadingAnchor),
            releaseDateLabel.trailingAnchor.constraint(equalTo: popularityLabel.trailingAnchor),
            releaseDateLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ])
    }
}
