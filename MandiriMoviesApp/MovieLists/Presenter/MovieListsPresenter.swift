//
//  MovieListsPresenter.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

class MovieListsPresenter {
    private let movieListsInteractor: MovieListsInteractor
    private let disposeBag = DisposeBag()
    
    private(set) var genreId = ""
    private(set) var pageNumber = 0
    
    private let eventShowMovies = PublishSubject<[Movie]>()
    var rxEventShowMovies: Observable<[Movie]> {
        return eventShowMovies
    }
    
    public init(movieListsInteractor: MovieListsInteractor) {
        self.movieListsInteractor = movieListsInteractor
    }
    
    public func setId(genreId: String) {
        self.genreId = genreId
    }
    
    func getMovies(pageNum: Int = 1, pageSize: Int = 10) {
        self.movieListsInteractor.getMovies(genreId: self.genreId, pageNum: pageNum, pageSize: pageSize)
            .subscribe(onNext: { [weak self] movieLists in
                guard let self else { return }
                guard !movieLists.isEmpty else { return }
                
                self.eventShowMovies.onNext(movieLists)
                self.pageNumber += 1
            }, onError: { _ in
                
            })
            .disposed(by: disposeBag)
    }
}
