//
//  MainViewController.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import SwinjectStoryboard
import RxSwift

class MainViewController: UIViewController {
    private let presenter = SwinjectStoryboard.defaultContainer.resolve(MainPresenter.self)
    private let disposeBag = DisposeBag()
    private var genres: [Genres] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    lazy var tableView: UITableView = UITableView()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show the navigation bar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        
        tableView = UITableView(frame: view.bounds, style: .plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        self.view.addSubview(tableView)
        
        setupPresenter()
    }
    
    private func setupPresenter() {
        presenter?.rxEventShowGenres
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] genres in
                self?.genres.append(contentsOf: genres)
            })
            .disposed(by: disposeBag)
        
        self.presenter?.getGenres()
    }
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genres.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = genres[indexPath.row].name ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = genres[indexPath.row].id else { return }
        let movieListController = MovieListsViewController(genreId: String(id))
        
        self.navigationController?.pushViewController(movieListController, animated: true)
    }
}
