//
//  DataCodableResponse.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 07/02/24.
//

import Foundation

public class DataCodableResponse<T: Codable>: Codable {
    public var genres: [T]?
    public var results: [T]?
    public var page: String?

    enum CodingKeys: String, CodingKey {
        case genres
        case results
        case page
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.genres = try? container.decode([T].self, forKey: .genres)
        self.results = try? container.decode([T].self, forKey: .results)
        self.page = try? container.decode(String.self, forKey: .page)
    }

    public var invalidResponseError: Error {
        return NSError(domain: "invalid response",
                       code: -1,
                       userInfo: [NSLocalizedDescriptionKey: "Invalid response"])
    }
}
