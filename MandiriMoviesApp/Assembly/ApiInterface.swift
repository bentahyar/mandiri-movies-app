//
//  ApiInterface.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 07/02/24.
//

import Foundation
import Alamofire
import RxSwift

class ApiInterface {
    static let shared = ApiInterface()
    
    private init() {}
    
    func makeRequest(url: URL, method: HTTPMethod, parameter: Parameters?) -> Observable<Data> {
        let headers: HTTPHeaders = [
          "accept": "application/json",
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5MjQ1NTBmZmI3YzgzYTU1NWEyYjJjNDIyNmE1NThjMyIsInN1YiI6IjU4MzNkMDA1YzNhMzY4MzM5OTAwNzRhMCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.SpgcelQWgtsW1YIshltZSFG6KadOyVEZCyYw3m9vtcw"
        ]
        
        return Observable.create { observer in
            let request = AF.request(url, method: method, parameters: parameter, headers: headers)
                .validate()
                .responseData { response in
                    switch response.result {
                    case .success(let data):
                        observer.onNext(data)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                    }
                }

            return Disposables.create {
                request.cancel()
            }
        }
    }
}
