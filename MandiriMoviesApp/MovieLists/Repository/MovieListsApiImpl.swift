//
//  MovieListsApiImpl.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift
import Alamofire

class MovieListsApiImpl: MovieListsApi {
    private let disposeBag = DisposeBag()
    
    func getMovies(genreId: String, pageNum: Int, pageSize: Int) -> Observable<[Movie]> {
        guard let url = URL(string: "https://api.themoviedb.org/3/discover/movie") else {
            return .empty()
        }
        
        let params: Parameters = ["with_genres": genreId,
                                  "page": pageNum,
                                  "pageSize": pageSize]
        
        return ApiInterface.shared.makeRequest(url: url, method: .get, parameter: params)
            .flatMap { jsonData -> Observable<[Movie]> in
                let datum: DataCodableResponse<Movie> = try JSONDecoder().decode(DataCodableResponse.self, from: jsonData)
                return Observable.just(datum.results ?? [])
        }
    }
}
