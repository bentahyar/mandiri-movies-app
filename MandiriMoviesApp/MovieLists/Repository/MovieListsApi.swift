//
//  MovieListsApi.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

protocol MovieListsApi {
    func getMovies(genreId: String, pageNum: Int, pageSize: Int) -> Observable<[Movie]>
}
