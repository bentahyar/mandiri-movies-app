//
//  DependencyContainer.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 07/02/24.
//

import Foundation
import Swinject
import SwinjectStoryboard

class DependencyContainer {
    static let shared = DependencyContainer()
    let container = SwinjectStoryboard.defaultContainer
    private init() {}

    func setup() {
        // Register your dependencies here
        container.register(MainApi.self) { _ in MainApiImpl() }

        container.register(MainInteractor.self) { _ in MainInteractorImpl(mainApi: self.container.resolve(MainApi.self)!) }

        container.register(MainPresenter.self) { _ in MainPresenter(mainInteractor: self.container.resolve(MainInteractor.self)!) }

        container.register(MovieListsApi.self) { _ in MovieListsApiImpl() }

        container.register(MovieListsInteractor.self) { _ in MovieListsInteractorImpl(movieListsApi: self.container.resolve(MovieListsApi.self)!) }

        container.register(MovieListsPresenter.self) { _ in MovieListsPresenter(movieListsInteractor: self.container.resolve(MovieListsInteractor.self)!) }
        
        container.register(DetailApi.self) { _ in DetailApiImpl() }

        container.register(DetailInteractor.self) { _ in DetailInteractorImpl(detailApi: self.container.resolve(DetailApi.self)!) }

        container.register(DetailPresenter.self) { _ in DetailPresenter(detailInteractor: self.container.resolve(DetailInteractor.self)!) }
    }
    
    func resolve<Service>(_ serviceType: Service.Type) -> Service? {
        return container.resolve(serviceType)
    }
}
