//
//  MainInteractor.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

protocol MainInteractor {
    func getGenres() -> Observable<[Genres]>
}
