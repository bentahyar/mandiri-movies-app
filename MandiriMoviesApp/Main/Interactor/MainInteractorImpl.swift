//
//  MainInteractorImpl.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

class MainInteractorImpl: MainInteractor {
    private let mainApi: MainApi
    
    public init(mainApi: MainApi) {
        self.mainApi = mainApi
    }
    
    func getGenres() -> Observable<[Genres]> {
        return self.mainApi.getGenres()
    }
}
