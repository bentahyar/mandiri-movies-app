//
//  MovieListsViewController.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import SwinjectStoryboard
import RxSwift

class MovieListsViewController: UIViewController {
    private let presenter = SwinjectStoryboard.defaultContainer.resolve(MovieListsPresenter.self)
    private let disposeBag = DisposeBag()
    private let genreId: String
    private var movies: [Movie] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    private var isLoadingData = false // Flag to track if data is currently being loaded
    
    var collectionView: UICollectionView!
    
    init(genreId: String) {
        self.genreId = genreId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show the navigation bar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 50)
        
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(MovieCell.self, forCellWithReuseIdentifier: MovieCell.reuseIdentifier)
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
        
        presenter?.rxEventShowMovies
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] movies in
                self?.movies.append(contentsOf: movies)
                self?.isLoadingData = false
            })
            .disposed(by: disposeBag)
        
        self.presenter?.setId(genreId: genreId)
        self.presenter?.getMovies()
    }
}

extension MovieListsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCell.reuseIdentifier, for: indexPath) as! MovieCell

        cell.titleLabel.text = movies[indexPath.row].title

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailViewController = DetailViewController(movie: movies[indexPath.row])
        
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}

extension MovieListsViewController: UIScrollViewDelegate {
    // Implement the UIScrollViewDelegate method to trigger loading the next page
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let visibleHeight = scrollView.frame.height
        
        // Check if the scroll view has reached the bottom
        if let pageNum = self.presenter?.pageNumber,
            offsetY + visibleHeight >= contentHeight,
            !isLoadingData {
            // Load the next page if data is not currently being loaded
            self.presenter?.getMovies(pageNum: pageNum + 1)
            self.isLoadingData = true
        }
    }
}
