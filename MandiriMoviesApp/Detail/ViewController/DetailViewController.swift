//
//  DetailViewController.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import UIKit
import SwinjectStoryboard
import RxSwift
import youtube_ios_player_helper

class DetailViewController: UIViewController {
    private let presenter = SwinjectStoryboard.defaultContainer.resolve(DetailPresenter.self)
    private let disposeBag = DisposeBag()
    private let movie: Movie
    private var reviews: [Review] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    @IBOutlet weak var playerView: YTPlayerView!
    @IBOutlet weak var tableView: UITableView!
    
    private var isLoadingData = false // Flag to track if data is currently being loaded
    
    init(movie: Movie) {
        self.movie = movie
        super.init(nibName:nil, bundle:nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show the navigation bar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        
        // Register custom cell
        tableView.register(DescriptionCell.self, forCellReuseIdentifier: "DescriptionCell")
        tableView.register(ReviewCell.self, forCellReuseIdentifier: "ReviewCell")

        // Register custom header view for the last section
        tableView.register(SnappingHeaderView.self, forHeaderFooterViewReuseIdentifier: "SnappingHeader")
        
        setupPresenter()
    }
    
    private func setupPresenter() {
        presenter?.rxEventShowReviews
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] reviews in
                self?.reviews.append(contentsOf: reviews)
                self?.isLoadingData = false
            })
            .disposed(by: disposeBag)
        
        presenter?.rxEventShowYoutube
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] video in
                guard let self else { return }
                
                playerView.load(withVideoId: video.key!)
            })
            .disposed(by: disposeBag)
        
        self.presenter?.setId(movieId: String(movie.id!))
        self.presenter?.getReviews()
        self.presenter?.getYoutubeVideo()
    }
    
    private func formatDate(_ dateString: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" // Assuming your date string is in this format
        if let date = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = "MMMM d, yyyy"
            return dateFormatter.string(from: date)
        }
        return nil
    }
}

extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in each section
        if section == 0 {
            return 1
        }
        return reviews.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            
            if reviews.isEmpty {
                cell.titleLabel.text = "No reviews available"
                cell.reviewLabel.text = ""
                cell.moviePosterImageView.image = nil
                return cell
            }

            // Configure the cell with data
            cell.titleLabel.text = reviews[indexPath.row].author
            cell.reviewLabel.text = reviews[indexPath.row].content

            // Set the movie poster image (You'll need to handle loading the image asynchronously)
            cell.moviePosterImageView.image = UIImage(named: "img_profile")

            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell", for: indexPath) as! DescriptionCell

        // Configure the cell with movie data
        cell.descriptionLabel.text = movie.overview
        if let popularity = movie.popularity {
            cell.popularityLabel.text = "Popularity: \(popularity)"
        }
        
        if let releaseDate = movie.releaseDate, let date = formatDate(releaseDate) {
            cell.releaseDateLabel.text = "Release Date: \(date)"
        }

        return cell
    }

    // MARK: - Table view delegate

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // If it's the last section, return the custom header view
        if section == 1 {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SnappingHeader") as! SnappingHeaderView
            // Configure your header view if needed
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 40
        }
        return 1
    }
}

extension DetailViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // If the last section is visible and the scrolling is slowing down, snap to the header
        let lastSection = tableView.numberOfSections - 1
        if lastSection >= 0 && scrollView.contentOffset.y > tableView.contentSize.height - scrollView.frame.size.height, velocity.y < 0 {
            targetContentOffset.pointee = CGPoint(x: 0, y: tableView.contentSize.height - scrollView.frame.size.height)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let visibleHeight = scrollView.frame.height
        
        // Check if the scroll view has reached the bottom
        if let pageNum = self.presenter?.pageNumber,
            offsetY + visibleHeight >= contentHeight,
            !isLoadingData {
            // Load the next page if data is not currently being loaded
            self.presenter?.getReviews(pageNum: pageNum + 1)
            self.isLoadingData = true
        }
    }
}
