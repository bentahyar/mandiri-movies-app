//
//  MovieListsInteractorImpl.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

class MovieListsInteractorImpl: MovieListsInteractor {
    private let movieListsApi: MovieListsApi
    
    public init(movieListsApi: MovieListsApi) {
        self.movieListsApi = movieListsApi
    }
    
    func getMovies(genreId: String, pageNum: Int, pageSize: Int) -> Observable<[Movie]> {
        return self.movieListsApi.getMovies(genreId: genreId, pageNum: pageNum, pageSize: pageSize)
    }
}
