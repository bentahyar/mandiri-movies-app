//
//  MainPresenter.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation
import RxSwift

class MainPresenter {
    private let mainInteractor: MainInteractor
    private let disposeBag = DisposeBag()
    
    private let eventShowGenres = PublishSubject<[Genres]>()
    var rxEventShowGenres: Observable<[Genres]> {
        return eventShowGenres
    }
    
    public init(mainInteractor: MainInteractor) {
        self.mainInteractor = mainInteractor
    }
    
    func getGenres() {
        self.mainInteractor.getGenres()
            .subscribe(onNext: { [weak self] genres in
                guard let self else {
                    return
                }
                
                self.eventShowGenres.onNext(genres)
            }, onError: { _ in
                
            })
            .disposed(by: disposeBag)
    }
}
