//
//  Genres.swift
//  MandiriMoviesApp
//
//  Created by Benedict Lukas Tahjar on 08/02/24.
//

import Foundation

struct Genres: Codable {
    let id: Int?
    let name: String?
}
